[![pipeline status](https://gitlab.fel.cvut.cz/kubelpe1/eShop/badges/master/pipeline.svg)](https://gitlab.fel.cvut.cz/kubelpe1/eShop/commits/master)

[![coverage report](https://gitlab.fel.cvut.cz/kubelpe1/eShop/badges/master/coverage.svg)](https://gitlab.fel.cvut.cz/kubelpe1/eShop/commits/master)

Authors: kubelpe1 & bergmpet

Some test are currently failing due to errors found in:
1) class ShoppingCart where both for-cycles (line 45 and 66) contains wrong stopping condition. Instead of "i<=0" should be "i>=0"
2) function getTotalPrice() in ShoppingCart should probably return float value instead of integer as all of the item prices are float values
3) function getDiscountedPrice() in class DiscountedItem does not divide percents by 100 so the price result of multiplication is incorrect. Possible change replacement of line 114 is: return super.getPrice()*((100 - discount)/100.0f);
4) method order.getTotalAmount() has variable int totalAmount (line 91) where floats are added, variable totalAmount must use type float